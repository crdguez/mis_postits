# Ruta por el norte - Planificación de viaje

* 28/7 Martes
    - Sabiñanigo comprar
    - Vitoria dormir (@park4night : http://park4night.com/lieu/6556/)[@park4night : http://park4night.com/lieu/6556/]
* 29/7 Miércoles
    - Vitoria visitar
    - Miranda - Patinar
    - Castro Urdiales - Dormir https://www.park4night.com/lieu/74646//castro-urdiales-16d-barrio-urdiales/spain/cantabria#.Xx03l9HtbQo
* 30/7 Jueves
   -  Castro - playa y dormir
* 31/7 Viernes
   - Playa de Berria - Dormir https://www.park4night.com/lieu/84228//arnuero-29b-ca-141/spain/cantabria#.Xx05DdHtbQo o camping? https://www.park4night.com/lieu/63652//santo%C3%B1a-3-carretera-faro-pescador/spain/cantabria#.Xx06KdHtbQo
* 1/8 Sábado
   - Loredo, Langre o Galizano Dormir https://www.park4night.com/lieu/90810//beranga-calle-sierra-llana/spain/cantabria#.Xx08QtHtbQo o https://www.park4night.com/lieu/90810//beranga-calle-sierra-llana/spain/cantabria#.Xx08QtHtbQo  
* 2/8 Domingo
    - San Vicente - Aparcar y dormir: https://www.park4night.com/lieu/52814//san-vicente-de-la-barquera-19-ca-236/spain/cantabria#.Xx09n9HtbQo
* 3/8 Lunes
    - Oviedo - Dormir https://www.park4night.com/lieu/22056//oviedo-258-calle-daniel-moyano/spain/asturias#.Xx1IstHtbQo o https://www.park4night.com/lieu/10114//lugones-30a-avenida-conde-santa-b%C3%A1rbara/spain/asturias#.Xx1IvdHtbQo
* 4/8 Martes
    - Tapia - Dormir https://www.park4night.com/lieu/8803//tapia-de-casariego-5-avenida-san-esteban/spain/asturias#.Xx1IfNHtbQo
* 5/8 Miércoles
    - Tapia
* 6/8 Jueves
    - Peñarronda - Dormir https://www.park4night.com/lieu/63261//castropol-2a-aldea-rubieira/spain/asturias#.Xx1ETdHtbQo
* 7/8 Viernes - ¿Torrelavega, patinar en la Lechera? Dormir en Liérganes ?https://www.park4night.com/lieu/13970//li%C3%A9rganes-14-calle-san-mart%C3%ADn/spain/cantabria#.Xx1LEdHtbQo
* 8/8 Sábado ?
