---
title: Información inicial  2ºBachillerato de Ciencias - Borrador
author: "IES Pedro Cerrada"
institute: "Departamento de Matemáticas"
fontsize: 16pt
titlegraphic: logosmall.png
theme: Warsaw
---


# Presentación

* Carlos Rodríguez
    - email: rodriguezjasoc@iespedrocerrada.org
    - web: [crdguez.gitlab.io](crdguez.gitlab.io)
* Información --> Web del centro --> Departamentos --> Matemáticas --> Carlos Rodríguez

---
# Enlaces de interés
### Normativa
* [Currículo de Bachillerato](https://educa.aragon.es/web/guest/-/norma-bachillerato)
<!---* [Currículo de la ESO](https://educa.aragon.es/web/guest/-/normativa-eso) --->

---
# Calendario escolar

## Provisional
* Inicio de exámenes 1a ev: 17/11
* Inicio de exámenes 2a ev: 9/2
* Inicio de exámenes 3a ev: 4/5 y 12/5

---
# Asignaturas

## Troncales
* Matemáticas, Biología y Química

## Optativas
* 4+3: fundamentos, geología + CTM, tic, psi2
* 3+3+1: fr, psi1 + CTM, tic, psi2 + tel, ef

---
# Programación
### Bloques de contenido

* Análisis (40%)
* Estadística (15%)
* Álgebra (30%)
* Geometría (15%)

---
# Horario

---

# Funcionamiento de Curso

* Dos exámenes por trimestre
* Primer examen a mitad evaluación y segundo examen a final de trimestre según calendario de Jefatura. 
* Nota de evaluación: Media ponderada a los pesos de la EVAU de los bloques que se hayan visto en la evaluación.
* Nota final: Media ponderada a los pesoso de EVAU de cada uno de los bloques 

---

# Funcionamiento de Curso

* Examen final de curso: Finalizada la 3ª evaluación, se realizará de manera obligatoria el examen final de toda la asignatura (simulará un examen de EVAU). Este examen permite recuperar bloques de contenidos pendientes o subir la nota media de aquellos alumnos cuya nota provisional fuera superior a cinco (en casos excepcionales podría bajarla). 

---

# Funcionamiento de Curso


* Convocatoria extraordinaria: A final de Junio, aquellos alumnos que no hubieran superado el curso podrán hacerlo mediante el examen correspondiente a dicha convocatoria.

---
# Funcionamiento de Curso

##  Otros temas


* Cambios de asignaturas: Hasta 19/9 vía expone/solicita
* Sigad Claves
* Cuadernillos --> 8 euros justos en metálico