# Convocatoria

* [Orden ECD/6/2021, de 15 de enero](https://educa.aragon.es/documents/20126/0/BRSCGI+%283%29.pdf/9ef11df9-8036-b5ce-5af0-271c80d9f551?t=1610972215100)
* [Carpeta de Educaragón](https://educa.aragon.es/web/guest/-/oposicion-sec)

# Normativa aplicable

* [Ley Orgánica 2/2006, de 3 de mayo, de Educación.](...)
* [Ley Orgánica 8/2013, de 9 de diciembre, para la Mejora de la Calidad Educativa.](...)
* []
