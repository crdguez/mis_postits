---
title: "Titulación ESO 2020-2021"
author: "IES Pedro Cerrada"
institute: "Jefatura"
theme: "Rochester"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 10pt
urlcolor: red
linkstyle: bold
aspectratio: 169
titlegraphic: img/portada.png
date:
section-titles: false
toc: false
navigation: "frame"
---

# Normativa
* [Normativa de ESO y Bachillerato](https://educa.aragon.es/-/normativa-eso-bachillerato?inheritRedirect=true&redirect=%2Fweb%2Fguest%2Fsearch%3Fq%3Dt%25C3%25ADtulo%2Bde%2Bgraduado)

En concreto:

* [De dónde venimos](https://elpais.com/sociedad/2013/11/26/actualidad/1385489735_160991.html)
* Última reforma: [LOMLOE](https://www.educacionyfp.gob.es/destacados/lomloe.html)
* La **LOMLOE** para 4º ESO entrará en vigor para el curso 22-23. Por tanto,
    - Este año se mantiene en vigor la anterior LOE
    - Se eliminan las reválidas previstas [(Artículo 6)](https://www.boe.es/boe/dias/2020/09/30/pdfs/BOE-A-2020-11417.pdf)
    - [**Instrucciones de titulación 2018**](https://educa.aragon.es/documents/20126/521996/84+Instrucceso.pdf/f76909c1-c5c6-b19b-72c9-f52f9f31536f?t=1578923172287)
    - [**ORDEN** **ECD/624/2018**, de 11 de abril, sobre la evaluación](https://educa.aragon.es/documents/20126/521996/83+ESO.pdf/31ced6a8-2551-2ee0-b142-902bf96b17d1?t=1578923170148)

