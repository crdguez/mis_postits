---
created: 2024-10-26T14:06:39+02:00
modified: 2024-11-22T19:10:32+01:00
---

# Proyecto cursillos

# A tener en cuenta

* Vienen al equipo porque los identifican en wp
* Cursillos poco dinámicos. Pocos largos por sesión.
* Piscina pequeña desaprovechada?
* Viernes a las 6 y media (22/11/24): 
       - pequeña: 6 personas, una calle. Todo el rato de uno en uno. El monitor los arrastra, con churros, colchonetas,... Dos calles vacías. A las 7 cursillo de bebés de 2años, con progenitores en el agua, 5 en toda la pequeña
       - grande: en una calle, 8 con Bea y 2 con Sara, niveles parecidos. Con Bea hacen 8 largos contados, sin material y apenas se mantienen a flote. Uno de los largos es ir de espalda remolcándose de la corchera. Los últimos 3 minutos a la pequeña a jugar.
