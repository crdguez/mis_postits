---
created: 2025-01-24T19:14:45+01:00
modified: 2025-01-24T20:36:43+01:00
---

# Reunión 24/01

- 5 de marzo
     - Olímpicos más socios de honor. Institucional, bandera olímpica. Un chico y una chica de la sección para el izado.
     - niños de 0-12 años de fuerapodrá venir gente de fuera al 5/3. 
     - Promoción en ampas de margen izquierda.
     - Empresa PAI, tipo rio y juego en helios, feria pirata.
     - De 11 a 14 y de 16 a 18 h. Habrá que promocionar las secciones esos días. Actividades propuestas al correo de Helios (para el viernes que viene).
- Plaza del Pilar sábado 24/05
    - Secciones que faltan que envíen proyecto
- Olimpiadas 01-04/05
    - Centenario 100% autofinanciado por patrocinadores 
    - Disciplinas que más se hacen en acedyr
    - Para la clasificación final se tendrían en cuenta las 4 secciones mejor puntuadas
    - Cada especialidad no durará 4 días 
    - Puede haber más de un equipo por club. Solo puntuará el primero por club.
    - Los de Helios no pagan inscripción.
    - Hay que pensar quiénes van
    - Se podrá comunicar internamente a partir del martes
    - Voluntarios para la competición serán necesarios 
    - Si se puede ayudar con la promoción mejor
    - Revisar la normativa de la olimpiada, para posterior reunión con Armando
    - Se han solicitado jueces a la fan
    -  Habría que confirmar con la fan que los tiempos sean oficiales ya que no está en calendario aprobado fan (yo creo que no habrá ningún problema)
