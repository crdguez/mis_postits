---
created: 2023-10-03T10:50:48+02:00
modified: 2023-10-03T10:50:48+02:00
---

# Acreditación de conocimientos en Matemáticas I de ciencias Sociales

En la reunión de departamento del 27 de septiembre de 2023 se trata el cambio de modalidad de la alumna **Abril Villegas** que ha decidido, en 2º de bachillerato, cambiar del itinerario de ciencias al itinerario de sociales.

Siguiendo la normativa vigente, para que el alumno pueda cursar las matemáticas de segundo de ciencias sociales es indispensable que el departamento acredite que los conocimientos de las matemáticas de sociales de primero han sido adquiridos por la alumna.

Puesto que el temario de matemáticas del itinerario de ciencias de primer curso cubre ampliamente los contenidos de las matemáticas de sociales de primer curso, **se acuerda unánimemente acreditar directamente los conocimientos de la asignatura a matemáticas de ciencias sociales I a la alumna Abril Villegas**.

Utebo a 27/09/20023
