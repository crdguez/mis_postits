---
# Datos
title: 
subtitle: 
author: 
lang: es

# Control
toc: False

# Formato
#documentclass: report
geometry:
- top=1in
- bottom=1in
- right=0.5in
- left=1.5in
mainfont: LiberationSans
fontsize: 12pt

# Bibliografía
#bibliography: referencias.bib
#csl: formato.csl
#link-citations: true
---

# Plan de recuperación de pendientes de la ESO

Para recuperar las matemáticas pendientes de primero, segundo o tercero de la ESO se seguirá el siguiente procedimiento:

* Durante los exámenes del curso actual se seleccionarán una serie de ejercicios para evaluar el grado de adquisición de los conocimientos de  la materia pendiente. Los ejercicios que se tendrán en cuenta para el apartado de pendientes serán indicados al principio del examen dentro de las instrucciones del mismo. Los ejercicios seleccionados se corresponderán siempre con contenidos del curso pendiente.

* En cada examen del alumno con las matemáticas pendientes, aparecerán dos notas. Una correspondiente al examen del curso actual y otra correspondiente a la evaluación de las matemáticas pendientes.

* El alumno superará las matemáticas pendientes siempre que la media de las notas correspondientes a la evaluación de pendientes sea mayor o igual que 5. 

\ 
------
\hrule

# Plan de recuperación de pendientes de Bachillerato

Se realizarán dos exámenes a lo largo del curso, previa realización de los ejercicios que serán entregados a los alumnos en las fechas indicadas.
El profesor de la asignatura y el jefe de departamento se encargarán de orientar el proceso de aprendizaje a lo largo del curso.

## 1^er^ EXAMEN

* Miércoles 20 de Septiembre reunión con los alumnos de 2º bachillerato con el área de matemáticas pendiente a las 12:20 en el laboratorio 1. Se les entregará las  hojas de ejercicios correspondientes al primer examen, y se les darán las instrucciones para su realización.
* El examen se realizará el **miércoles 13 de Diciembre**. La fecha puede ser modificada para coordinarnos con otros exámenes de pendientes.

	
### CONTENIDOS DEL 1^er^ EXAMEN

#### MATEMÁTICAS I

* Aritmética y Álgebra
* Trigonometría

#### MATEMÁTICAS APLICADAS A LAS CIENCIAS SOCIALES I

* Aritmética y Álgebra
* Estadística

## 2º EXAMEN

* Miércoles 21 de Diciembre se entregarán las hojas correspondientes al segundo examen, y se darán las instrucciones para su realización.
* El examen se realizará el **Miércoles 29 de Marzo**. La fecha puede ser modificada para coordinarnos con otros exámenes de pendientes.
	

### CONTENIDOS DEL 2º EXAMEN

#### MATEMÁTICAS I

* Números complejos
* Estadística y Probabilidad
* Geometría analítica
* Funciones

#### MATEMÁTICAS APLICADAS A LAS CIENCIAS SOCIALES I

* Probabilidad
* Funciones.


La calificación final se obtendrá como media aritmética de los dos exámenes, siempre y cuando se alcance una calificación mínima de 3 en cada uno de ellos.
