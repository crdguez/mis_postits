# Asignatura: Matemáticas para la toma de decisiones

- Contenido:
  1. Aritmética modular y criptografía
     - Números primos y teorema fundamental de la aritmética
     - Algoritmo RSA y criptografía
     - Conjunto Z/mZ y función phi de Euler
  2. Teoría de grafos
     - Definiciones básicas y propiedades
     - Formas de representar un juego: extensiva y normal
     - Algoritmos de grafos: coloración, Fleury, Dijkstra
  3. Teoría de juegos
     - Conceptos básicos: juegos de suma cero, estrategias puras y mixtas
     - Resolución de juegos de dos jugadores con suma cero e información perfecta
- Dificultad y requisitos:
  - Requiere una sólida base matemática para superarla con garantías.
  - Exige un alto nivel de estudio y dedicación constante debido a su complejidad.
  - Implica un compromiso significativo para dominar conceptos esenciales en la toma de decisiones en situaciones cotidianas y problemas reales.

