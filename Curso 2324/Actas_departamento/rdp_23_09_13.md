---
created: 2023-09-ne-11-08T17:21:02+01:00
---

## 23/09/13

Se tratan los siguientes temas:

- Respecto a la opinión del departamento respecto a si las reuniones hay que hacerlas de manera telemática, el resultado de la votación ha sido el siguiente:

	*  Sesiones de evaluación: de manera unánime se decide que la primera y la segunda sean de manera telemática y las finales de manera presencial y en el caso de que tengan que ser todas del mismo tipo  todas  telemáticas. 
	*  Claustros: De manera unánime todas de forma telemática
	*  CCP: De manera unánime todas telemáticas

- Alumnos pendientes de bachillerato: Se decide convocar a los alumnos para la semana siguiente para informarles y entregarles las fichas que tienen que hacer para seguir el procedimiento ordinario de recuperación.

- Se informa que desde conserjería quieren llevar un control del número de cuadernillos que se han hecho de bachillerato

- José Manuel solicita que se pida Anaya o un libro de matemáticas en inglés de tercero de la ESO para poder atender mejor al alumno que ha venido de Inglaterra y que no sabe castellano.

- Se informa que este año las programaciones tienen que estar para finales de marzo, sin embargo desde el departamento de extraescolares han pedido que adelantemos las actividades que vayamos a realizar. Ningún miembro del departamento ha mostrado interés por organizar alguna.
