## 24/06/12

Se comenta la CCP de 2024/06/05:

* Se aprueba el acta de la ccp de mayo
* Entrega de notas a las 10:00 del 21/06. Los autobuses salen a las 11:10
* Plan de refuerzo personalizado: Alumnado que promociona con asignaturas pendientes. Solo 
* Plan de seguimiento personalizado de alumnado que no promociona y alumnos que vayan suspendiendo. Este año se hace la apertura (falta la plantilla) 
* Mandarán un correo con la información y pautas
* Memoria final de curso: adjuntan un guión orientativa
* Se están haciendo reuniones con los colegios para recoger información de los nuevos alumnos
* Visita de familias de los colegios de los alumnos adscritos el 3 julio
* Proyecto de convivencia: Gimkana y cartas de los de 1º de la ESO a los alumnos de 6º de Primaria
* Sesiones de evaluación: Mandarán correo con el calendario. Las derivaciones definitivas se tomarán en las evaluaciones finales
* Banco de libros: Recogida el viernes 14 de junio 4 ESO y probablemente 3 ESO, resto los días siguientes
* Jornada lectiva: 18 horas, 16 lectivas mínima. Si no 19. 
* Cupo: Desdobles a partir de 25 alumnos darán una hora por grupo. Hasta ahora era a partir de 22. 23-24 lo dejan congelado, igual las dan cuando empiece el curso. Jefaturas departamento (los de 3 tendrán 2). Los de 55 años, la mitad ahora y ¿la otra mitad en septiembre?
* Informe de extraescolares. Prácticamente no hay porque estamos a final de curso.
* Formación: Hay que recoger el voto telemático para poder solicitar los cursos de bienvenida. Haciéndolo en el claustro final estaríamos fuera de plazo. Se enviará un correo, la no respuesta se considerará que se está a favor. Para votar en contra habrá que contestar a ese correo
* Propuestas de las comisiones. Como no ha habido aportaciones, salvo en la de la básica, se aprueban todas las propuestas del PCE, PCB y protocolo de Extraescolares. El plan de Mejora de la Básica se ha modificado, se nos enviará por correo (acompañamiento personal para el alumnado, inclusión de profesores de FP en el plan de convivencia, ...)
