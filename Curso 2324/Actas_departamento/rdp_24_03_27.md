## 24/03/27

* Los resultado del segundo examen de pendientes han sido:

|Alumno|Nota 2ªev|
|:----|:----|
|Daniel Velilla|1,21|
|Iván Miguel|3,27|
|Mario Muñoz|2,34|
|Manuel Amar|5,32|

* Las medias de los alumnos de 1ºde Bachillerato quedan:

|Alumno|Nota 1ªev|Nota 2ªev|Nota|
|:----|:----|:----|:----|
|Daniel Velilla|3,31|1,21|2,26|
|Iván Miguel|3,16|3,27|3,21|
|Mario Muñoz|2,52|2,34|2,43|
|Manuel Amar|5,08|5,32|5,20|

* Se acuerda enviar dichos resultados al tutor
