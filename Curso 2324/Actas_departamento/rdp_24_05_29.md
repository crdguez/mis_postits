## 24/05/29

- Seguimiento de la programación:
- Fecha: 22/05/24

| Curso               | Seguimiento                                                  |
| ------------------- | ------------------------------------------------------------ |
| 1º A                | Álgebra                                                      |
| 1º B                | Mañana 23/05 examen de álgebra                               |
| 1º C                | Geometría. Ya he dado áreas y perímetros de figuras, voy a empezar con Pitágoras |
| 1º D                | Geometría. Ya he dado áreas y perímetros de figuras, voy a empezar con Pitágoras |
| 1º E                | Mañana 23/05 examen de álgebra                               |
| 2º A                | Hecho el examen de sistemas. Mañana jueves 23/05  comenzamos geometría. |
| 2º B                | Hecho el examen de sistemas. Teorema de pitágoras aplicado a perímetros y áreas de figuras planas. |
| 2º C                | Teorema de pitágoras aplicado a perímetros y áreas de figuras planas. |
| 2º D                | Hecho el examen de sistemas. Teorema de pitágoras aplicado a perímetros y áreas de figuras planas. |
| 3º A                | Geometría. Ya he dado pitágoras, áreas y perímetros de figuras. Voy a empezar con volumen. |
| 3º B                | Áreas de cuerpos geométricos.                                |
| 3º C                | Áreas de cuerpos geométricos.                                |
| 3º D                | Geometría. Ya he dado pitágoras, áreas y perímetros de figuras. Voy a empezar con volumen. |
| 4º A Mat. A         | Inecuaciones de segundo grado.                               |
| 4º A Mat. B         | Ecuaciones de la recta                                       |
| 4º B                | Probabilidad condicionada (la geometría analítica la he dejado para las últimas tres semanas con objeto de poder atender mejor al alumnado de Cruzando Fronteras) |
| 4º C                | Probabilidad condicionada (la geometría analítica la he dejado para las últimas tres semanas con objeto de poder atender mejor al alumnado de Cruzando Fronteras) |
| 4º D                | Tema de Semejanza                                            |
| 1º Bach CIE         | Probabilidad                                                 |
| 1º Bach CIT         | Probabilidad                                                 |
| 1º Bach CCSS        | Variable continua. La normal                                 |
| 2º Bach Mat 2 - CIE | Completado el curso                                          |
| 2º Bach Mat 2 - CIT | Completado el curso                                          |
| 2º Bach Mat CCSS    | Todos los bloques de contenidos del curso han sido vistos en clase |