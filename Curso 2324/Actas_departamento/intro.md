# Introducción

El siguiente documento contiene las actas del  departamento de matemáticas del IES Pedro Cerrada siguiendo las instrucciones que aparecen en la siguiente normativa:

*  Real Decreto 83/1996, de 26 de enero, por el que se aprueba el Reglamento Orgánico de los Institutos de Educación Secundaria. [^1]
* BOA, «ORDEN de 18 de mayo de 2015, de la Consejera de Educación, Universidad, Cultura y Deporte por la que se aprueban las Instrucciones que regulan la organización y el funcionamiento de los Institutos de Educación Secundaria de la Comunidad Autónoma de Aragón.» [^2]

[^1]: [https://www.boe.es/buscar/pdf/1996/BOE-A-1996-3834-consolidado.pdf](https://www.boe.es/buscar/pdf/1996/BOE-A-1996-3834-consolidado.pdf)
[^2]: [https://educa.aragon.es/documents/20126/521996/218+ORDEN+IOF+IES+boa%281%29.pdf/8cf0ff03-d0b5-b3f3-19c5-7ebf57d9de60?t=1578923050654](https://educa.aragon.es/documents/20126/521996/218+ORDEN+IOF+IES+boa%281%29.pdf/8cf0ff03-d0b5-b3f3-19c5-7ebf57d9de60?t=1578923050654)




# Reuniones de departamento



