## 23/09/27

Se tratan los siguientes temas:

* Alumnos con las matemáticas pendientes: se entrega el listado de pendientes facilitado por jefatura. Se comenta que hay que subir a la web el procedimiento de recuperación de pendientes, para ello se comenta que se va a realizar un documento con dicha información.
* Cambio de modalidad de una alumna de 2º de bachillerato (Abril Villegas): La alumna que se ha cambiado a ciencias sociales muestra su preocupación por el hecho de que las matemáticas de primero de sociales no las ha cursado y por tanto las tendrá convalidadas. Se revisa en el departamento la normativa y se comprueba que las asignaturas convalidadas no influyen el expediente académico. En cualquier caso, el departamento tiene que acreditar que la alumna posee los conocimientos necesarios para poder cursar la nueva asignatura. Se decide hacer el documento acreditativo ya que las matemáticas cursadas en 1º de bachillerato de ciencias son de un nivel superior a las matemáticas de 1º de ciencias sociales.
