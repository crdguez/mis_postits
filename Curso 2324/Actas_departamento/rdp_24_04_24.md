## 24/04/24

Se comenta la CCP del 10/04:

* Ha venido el inspector y ha solicitado las programaciones. Esta prevista una actuación censal de las programaciones de los centros de las asignaturas de matemáticas y lengua de 2ºESO
* Se comenta información delmprceso de escolarización (posterior al de adscripción). Se hará una charla el 17/04 por streaming desde el CIFE Juan de Lanuza
* Se informa del calendario provisional de final de curso
* La implantación de la normativa sobre el uso de móviles se valora positivamente. Ha habido pocas incidencias y los padres están siendo receptivos.
* Se recuerda que desde Secretaría se solicita información sobre qué libros se tienen previstos cambiar. Hay que comunicarlo antes del 15/05. El departamento de matemáticas ha decidido no cambiar de libros. De la ESO se cambiaron el año pasado y de Bachillerato se pretende seguir con los cuadernillos
* Plan digital de Centro: Se ha apuntado el 25% del claustro, por lo que se certificará el tercer y último taller previsto en el plan
* Se comentan los talleres de la jornada cultural del 16/04 que este año está enfocada en el concepto de la luz
* Del programa Erasmus se vuelve a repetir lo que ya se comentó en el Claustro 
* Se informa de las extraescolares previstas para las próximas semanas
