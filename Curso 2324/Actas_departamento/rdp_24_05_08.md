## 24/05/08

* Se planifican los exámenes de pendientes de la ESO y se comenta que se ha solicitado fecha y aula para su elaboración. Se decide que cada profesor informe a sus alumnos de la fecha y aula que finalmente se nos asigne
* Se comenta que se ha empezado a informar en las aulas de 3ºESO sobre la asignatura optativa de 4ºESO Matemáticas para la toma de decisiones 
* Se decide incluir dicha información en el documento de asignaturas optativas que utiliza Jefatura de Estudios para informar a alumnos y familias

