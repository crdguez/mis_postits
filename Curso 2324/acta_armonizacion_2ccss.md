# Acta armonización

## 26/10/2023

* Elena y Javier co-armonizadores
* María Ángeles adjunta
* 7.5 88%
* Futuro: Cualquier calculadora + Geogebra
* 6 preguntas: 2 matrices y PL. 2 análisis: 1 resolución de problemas, 1 ejercciio. 2 preguntas de estadísticas: Incertidumbre, Distribución de probabilidad, Inferencia
* Entran de nuevo las distribuciones de probabilidad
* Normal y Binomial
* Se responde las 3 preguntas que se quieran. Hay que indicar qué preguntas se resuelven.
* No hay grafos este año, pero habrá.
* Nuevo para este año: Potencias de matrices, Teor R-F
* Método de simplex, no
* PL: Soluciones enteras
* Asíntotas, errores en la aproximación
* Derivadas n-esima
* 00-00, oo/oo, 0/0 solo si se pueden simplificar
* derivabilidad no
* Pueden aparecer gráficas
* Binomial, normal, aproximación de la binomial por la normal. Calcular valor a partir de la probabilidad.
* Pueden calcular los parámetros con la calculadora, pero tienen que decir qué han hecho
* A^C, suceso complementario mejor que contrario
* No hay preguntas específicas. Los dos problemas pueden combinar los tres contenidos de estadística
