---
created: 2024-02-07T16:37:28+01:00
modified: 2024-02-07T16:37:28+01:00
---

# CCP 07 02 2024

# Informe de dirección: 
- Adscripción: Se estiman 19 repetidores. Hasta 120 del cupo habría que baremar.
- Pruebas de acceso a grados
- Bachillerato:
    - evau preinscripción del 22/01  a 16/2
    - fechas: 4, 5 y 6 de junio 16 de mayo evaluación final ordinaria
- FP: graduación, Jerónimo hablará con los departamentos para ver cuántos y cuándo hacerla
- Equipos educativos de febrero. Habrá que rellenar del  12 al 15 de febrero
- Plan digital de centro
- Pruebas BRIT:
