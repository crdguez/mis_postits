---
created: 2023-09-04T07:34:39+02:00
modified: 2023-09-04T12:14:49+02:00
---

## ccp_23_09_04

* Aprobación, si procede, del acta de la CCP anterior
    - Se aprueba
* Informe de Dirección
    - 98 profesores (3 sin adjudicar de electrónica )
    - María Mainer nueva orientadora
    - Se revisa el calendario de inicio de curso
    - **Miércoles formación TEA**
    - **Jueves, claustro y entrega de horarios**. **A las 12:10 salen los autobuses**
    - Martes 12, 2 primera hora jefatura-recepción, clases normales a 2a hora. 1 Bach 10 35 recepción en gimnasio por jefatura, luego con los tutores hasta las 12:20, luego clase normal
    - Calendario:
      -  ![Image](./image_1_1.png) 
      - ![Image](./image_1_2.png) 
      - ![Image](./image_1_3.png ) 
    - Fechas finales de 2bach pendientes del calendario de la universidad
    - Claustro extraordinario por elecciones a consejo escolar
    - Estructura de grupos
    - Programaciones didácticas 27/03
    - Centro preferente TEA impuesto por la Administración. Amelia pt en comisión. 2 alumnos de 1eso. Hay formaciones específica en Doceo y el miércoles 7 a las 12 en um1
    - En el claustro de octubre explicarán la renovación del consejo
    - Procedimiento para solicitar reduccion 55 interinos
    - **Rellenar el formulario de Datos**
    - **Sigad, gestión de incidencias. En portal del profesorado está el protocolo (disciplina)**
    - Laboratorio 1 habilitado como informática
    - Cambio de timbre. Se han cambiado, volumen regulable
    - Máquina de vending de electrónica eliminada
* Departamento de Extraescolares
    - Formulario para registrar actividades extraescolares
    - Importante registrar ahora las que se tengan previstas
    - Se pueden consultar en Calendar
    - Las actividades en Utebo no necesitan autorización, ya la dan en la matrícula 
* Departamento de Formación e Innovación
   - Tutorización de máster que hablen con Pedro, o si se quiere organizar algún curso
   -  Tienen presupuesto para Erasmus
   -  Plan digital, tiene que haber un 80% del profesorado con alguna certificación

* Ruegos y preguntas
    - Santiago: solicita planificar unas jornadas culturales y de convivencia. Sugerencias a él. Tema La luz. 1a semana de febrero.
    - Natalia: Tienen concedidos 2 auxiliares (uno para inglés y otro para francés)
    - Esther pregunta por las evaluaciones online. La normativa no contempla poder hacerlas. Ccps claustros y consejos sí. **Recoger la opinión de los departamentos (votación) para consultar a inspección (claustro y evaluaciones)**
    -
