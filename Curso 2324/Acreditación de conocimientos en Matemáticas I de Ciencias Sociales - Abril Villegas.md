---
created: 2023-10-03T10:50:48+02:00
modified: 2023-10-03T11:20:30+02:00
---

# Acreditación de conocimientos en Matemáticas I de Ciencias Sociales - Abril Villegas

En la reunión de departamento del 27 de septiembre de 2023 se trata el cambio de modalidad de la alumna **Abril Villegas** que ha decidido, en 2º de bachillerato, cambiar del itinerario de ciencias al itinerario de sociales.

Siguiendo la normativa vigente, para que la alumna pueda cursar las matemáticas de segundo de ciencias sociales es indispensable que el departamento acredite que los conocimientos de las matemáticas de sociales de primero han sido adquiridos.

Puesto que el temario de matemáticas del itinerario de ciencias de primer curso cubre ampliamente los contenidos de las matemáticas de sociales de primer curso y la alumna superó el curso con una 6 de nota, **se acuerda unánimemente acreditar los conocimientos de la asignatura Matemáticas I de Ciencias Sociales a la alumna Abril Villegas** .

\vspace{2cm}

*Utebo a 27/09/2023,*  \
*El jefe del departamento de Matemáticas,*  \
*Carlos Rodríguez*
 
