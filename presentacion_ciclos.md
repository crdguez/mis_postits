% Carlos Rodríguez - 2020
% pandoc -t beamer input.md -o output.tex 
% pandoc -t beamer input.md -o output.pdf



# Normativa y web de Admisión

- [ORDEN ECD/412/2020, de 27 de mayo](http://www.boa.aragon.es/cgi-bin/EBOA/BRSCGI?CMD=VEROBJ&MLKOB=1116086482323&type=pdf)
- [Admisión y matrícula](https://educa.aragon.es/-/formacion-profesional/calendario/admision-y-matricula)


# Instrucciones

- [**Presentación de solicitudes**](https://educa.aragon.es/-/realizacionsolicitudes): del **viernes 19 al jueves 25 de junio de 2020**, ambos inclusive. Se presentará **telemáticamente**, lo que implica que no es necesario acudir presencialmente al centro a realizar ningún trámite. 

- Documentación a entregar: **certificación académica**

- Grupo  1:  **60%  de  las  plazas  ofertadas**  para  las  personas  que  accedan  por  poseer  el  **Título de Bachillerato** o equivalente a efectos académicos.

- Existen **preferencias de acceso para las modalidades y materias** que facilitan la conexión con el ciclo formativo. ([tabla de materias vinculadas](https://educa.aragon.es/documents/20126/430824/TABLA+B%29+ANEXO+II+Materias+vinculadas+BachCFGS.pdf/c398186a-332c-8e4b-1b1f-bc68f3f954f3?t=1573120755591))

- La prioridad viene determinada por la mayor **nota media aritmética** del expediente académico en Bachillerato y la **mayor calificación  obtenida en la materia de bachillerato vinculada**.

  

# Instrucciones (continuación)

-  [**Oferta de plazas de cada centro**](https://educa.aragon.es/-/listasvacanteserg) (del **17 de junio**, a partir de las 16 h., al 25 de junio, inclusive)
-  [**Notas de corte C.F.G.S.**](https://educa.aragon.es/documents/20126/554446/2019-06-14_+Nota+de+Corte+CFGS+2018-2019.xls/c036e0ac-d527-c561-d80e-b4bb7ba6089f?t=1580369166652) de cursos anteriores
-  Publicación de las **listas provisionales** de admitidos, no admitidos y excluidos: Miércoles 8 de julio
-  Publicación de [**listas definitivas de admitidos**](https://educa.aragon.es/-/listasadjudicaciones), no admitidos y excluidos: **Miércoles 15 de julio** 
-  **Matriculación del alumnado admitido** de forma definitiva en este proceso de admisión:  **Del jueves 16 al miércoles 22 de julio, ambos inclusive**. La matrícula podrá realizarse presencialmente o telemáticamente, según determine **el centro**.