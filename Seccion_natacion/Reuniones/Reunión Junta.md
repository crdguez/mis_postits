---
created: 2024-09-13T19:05:25+02:00
modified: 2024-09-13T20:43:05+02:00
---

# Reunión Junta 13/09

* Agradecimientos a la labor de los delegados 
* Agenda centenario:
    -  Jesús Cerbero y Javier Gascón responsables del programa
    -  Abierto a las ciudad
    -  Pilar, CNH, río. Punto de inflexión 
    -  Valores 
    -  Orígenes 
    -  8500 socios actualmente 
    -  Zubero bronce 
    - Camilo Santiago 3 de Europa (Kilez dixit)
    - Compromiso medioambiental (ayuntamiento che ...)
    - Financiación 0
    - Eventos, se han pedido unos cuantos . Nosotros obviamente no podemos, no tenemos instalación acorde para hacer cpto España (esto lo digo yo)
    - Inauguración en teatro principal, presentada por León Harlem
    - Van a dar invitaciones a las secciones, hay que pensar quién va
    - 5 de marzo, paseo centenario, pebetero y bandera olímpica 
    - 1 a 4 de mayo olimpiadas escolares con los acedyr. Nos interesa? Desfile de inauguración por las calles de Zaragoza 
    - 24/25 de Mayo. Plaza del Pilar promoción de secciones
    - 13/06 carrera de patos solidaria (se ha hecho en Bilbao). Los patos se compran vía tickets.
    - 29/30 de junio, música y deporte en el río. Pondrán una plataforma en Helios
    - Para el Pilar ofrenda 
    - 23/07 gala de celebración del centenario, es la más importante. Hay que pasar nombres, incluso gente fallecida (no hace falta que sean deportistas). Aura y fuera del aura. Fuegos artificiales 
    - 9 de agosto descenso del sella dedicado a Helios
    - Van a intentar traer a la selección femenina de waterpolo
    - Un triatlón es septiembre 
    - 4 de octubre remo Oxford masculino y femenino 
    - 26 de octubre, 10k, 5k, andada popular
    - 11 de diciembre, gala de clausura, Palacio de congresos? B vocal, Joaquín Reyes
    - Objetivo ganar socios y patrocinios
    - Piden un interlocutor por sección y del club Armando, pasarlo a vicepresidente 
    - Se van a necesitar voluntarios, habrá que pedir
    - Esto lo pienso yo, salvo la de patinaje no hay ninguna mujer en la reunión. La próxima persona delegada de la sección de Helios tiene que ser mujer
    -
