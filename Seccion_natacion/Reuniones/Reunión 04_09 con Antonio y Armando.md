---
created: 2024-09-04T19:32:27+02:00
modified: 2024-09-04T21:46:43+02:00
---

# Reunión 04/09 con Antonio y Armando

- Alberto Echevarría, sacar licencia para que pueda firmar las prácticas 
- La piscina sigue igual, se va a intentar hablar con Pili Tejela a ver si se puede convencer al ayuntamiento. Se seguirá peleando la academia. Se va a mirar también Utebo, cuarte, Liceo
- Documentación para los contratos
- Marcos su pago como mejor le vaya: factura, etc
- Prorratear licencia a los 10 meses de cuota
- Fórmula para anticipar pagos de campeonatos
- Cursillos, hay que intentar  que Marc marque las pautas. Se puede hablar con Clara
- Material: Barra y Gimnasio, ropa de entrenadores. Hablar con Mariano para las corcheras y que le pida a Antonio.
- Social 28 de septiembre. Se solicita reservar piscina
- Ofrecimiento de piscina para las jornadas de tecnificación: sin problema siempre que no sea el sábado por la mañana y si la federación paga. Wp ya paga
