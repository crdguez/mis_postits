---
created: 2024-09-03T18:26:12+02:00
modified: 2024-09-03T19:57:51+02:00
---

# Reunión 3/9/24 sección de natación

- Gimnasio los lunes es posible. La barra. La vieja sala de secciones
- Usuario y contraseña de juegos escolar.
- La piscina de última hora
- Telegram para los grupos
- Correos de los entrenadores en los comunicados, que no se sienten informados
- Asistencias porcentajes
- Bea y Laura en cursillos de captación 
- Gorros y bañadores
- Fiesta 28: Detalles para los entrenadores. Se regala gorro este año?
- Ropa para entrenadores
