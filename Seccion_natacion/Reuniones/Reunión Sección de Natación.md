---
created: 2024-04-01T18:56:40+02:00
modified: 2024-04-01T19:03:55+02:00
---

# Reunión Sección de Natación

## Presentación

* Jesús Malón
* Pepe Salueña
* Carlos Rodríguez

## Misión

* Recuperar la confianza de la comunidad natatoria
* Mejorar las condiciones de entrenamiento
* Servir de nexo entre la sección y la junta deportiva
*
