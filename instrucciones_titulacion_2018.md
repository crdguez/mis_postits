---
title: "Instrucciones Titulación ESO 2020-2021"
author: "IES Pedro Cerrada"
institute: "Jefatura"
theme: "Rochester"
colortheme: "beaver"
fonttheme: "professionalfonts"
mainfont: "Hack Nerd Font"
fontsize: 10pt
urlcolor: red
linkstyle: bold
aspectratio: 169
titlegraphic: img/portada.png
date:
section-titles: false
toc: false
navigation: "frame"
---

# Instrucciones

GOBIERNO . DE AAAGON
INSTRUCCIONES DE 15 DE MAYO DE 2018, DEL DIRECTOR GENERAL DE PLANIFICACIÓN Y
FORMACIÓN  PROFESIONAL,  SOBRE  LA  OBTENCIÓN  DEL TÍTULO  DE  GRADUADO  EN EDUCACIÓN SECUNDARIA OBLIGATORIA EN LA COMUNIDAD AUTÓNOMA DE ARAGÓN

El  Real  Decreto-ley  5/2016, de 9 de diciembre, establece la  adecuación  del régimen jurídico de las evaluaciones finales de etapa y modifica las condiciones para la obtención de los títulos de Graduado en Educación Secundaria Obligatoria y Bachiller. Para el resto  de las enseñanzas en las que estuviera prevista la posibilidad de realizar dichas evaluaciones y a las que el Real  Decreto-ley 5/2016, de 9 de diciembre no se refiere de forma directa, debe tenerse en cuenta que, siempre que esto no se oponga a la normativa que continúa vigente, la interpretación de la norma deberá hacerse entendiendo, en último término, que las modificaciones introducidas en dicho calendario principalmente están orientadas a garantizar al alumnado de las distintas enseñanzas el mantenimiento de las condiciones de titulación existentes antes de la  modificación de la Ley Orgánica 2/2006, de 3 de mayo, de Educación, realizada por la Ley Orgánica 8/2013, de 9 de diciembre.
El Estado, con las competencias que tiene atribuidas para  la regulación de las condiciones de obtención, expedición y homologación de los títulos académicos y profesionales, publica el Real Decreto 562/2017, de 2 de junio, por el que se regulan las condiciones para la obtención de los títulos de Graduado en Educación Secundaria Obligatoria y de Bachiller, de acuerdo con lo dispuesto en el Real Decreto-Ley 5/2016, de 9 de diciembre, de medidas urgentes para la ampliación del calendario de implantación de la Ley Orgánica 8/2013, de 9 de diciembre, para la mejora de la calidad educativa.
En lo que afecta a la obtención del título de  Graduado en Educación Secundaria Obligatoria, el citado Real Decreto.562/2017, de 2 de junio, introduce modificaciones que mantienen las condiciones de titulación existentes antes de la modificación de la Ley Orgánica 2/2006, de 3 de mayo. Estas modificaciones afectan a la capacidad del equipo docente para valorar de manera global, y como hasta ahora se venía haciendo, si el alumno o alumna ha adquirido las competencias y objetivos  de la  etapa  para  realizar  la  propuesta  de titulación cuando el alumno no ha superado todas las materias.
Para articular las condiciones de titulación marcadas por el citado Real Decreto 562/2017, de 2 de junio, para el curso 2016/2017 se publicaron las instrucciones de 8 de junio de 2017, del Director General de Planificación y Formación Profesional, sobre la obtención del título de graduado en Edu cación Secundaria Obligatoria en la Comunidad Autónoma de Aragón. A partir de la evaluación de desarrollo y aplicación de las mismas, y con el fin de conseguir una mayor equidad en la aplicación de estos criterios que establece el citado Real Decreto 562/2017,
 de 2 de junio, se dictan las siguientes

INSTRUCCIONES

- Primera. 

Para la obtención del título de Graduado en Educación Secundaria Obligatoria se estará a lo dispuesto en el apartado 1 del artículo 2 del Real Decreto 562/2017, de 2 de junio.

- Segunda.

En consecuencia con lo anterior, y en el caso en el que los alumnos hayan obtenido evaluación negativa en un máximo de dos materias, siempre que estas no sean de forma simultánea Lengua Castellana y Literatura, y Matemáticas; podrán obtener el título de Graduado  en  Educación  Secundaria  Obligatoria  si  el  equipo  docente  considera  que  han alcanzado los objetivos de la etapa y han adquirido las competencias correspondientes. En esta decisión el equipo docente tendrá en cuenta que el alumno tenga  posibilidades de continuar cualquiera de los estudios post-obligatorios del Sistema Educativo.

- Tercera. 

Siempre y cuando el equipo docente, atendiendo a los criterios establecidos normativamente, considere expresamente que el alumno reúne en la convocatoria ordinaria de junio las condiciones para la obtención del título, se certificará esta circunstancia a los únicos efectos de admisión de alumnos en enseñanzas postobligatorias.  ·
Esta certificación reflejará las materias superadas y no superadas, así como la nota media correspondiente, que incluirá las calificaciones obtenidas en el momento de la convocatoria ordinaria en todas las materias cursadas.
En todo caso, el proceso de evaluación de estos alumnos se cerrará en la convocatoria extraordinaria de septiembre, en la que se hará la correspondiente propuesta  formal de título de Graduado en Educación Secundaria Obligatoria, teniendo en cuenta que se mantienen las condiciones ya acreditadas en la evaluación ordinaria. La nota media que constará en el título será la calculada teniendo en cuenta las calificaciones de todas las materias cursadas tras la convocatoria  extraordinaria.

- Cuarta.

Quedará sin efecto, en lo que se refiere a la nota media mínima exigible para la obtención del título de graduado en Educación Secundaria Obligatoria, lo establecido en apartado decimoséptimo.2 de la Resolución de 7 de diciembre de 2016, del Director General de Planificación  y  Formación  Profesional,  por  la  que  se concreta  la evaluación  en  Educación
Secundaria Obligatoria en los centros docentes de la Comunidad Autónoma de Aragón.



Zaragoza, 15 de mayo de 2018

EL DIRECTOR GENERAL DE PLANIFICACIÓN Y FORMACIÓN PROFESIONAL

EL SECRETARIO GENERAL TÉCNICO
(p.s. Orden 4 de agosto de 2015)