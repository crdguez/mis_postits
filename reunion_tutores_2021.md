 P# 17/10

* Insistir en la distancia y comen
* Pizarra y libros. 
* Economía bi (fyq economía bi), fyq y biología (fyq) -- cambiar economía por biología
* Baños que hagan fila
* A arte no se da cambio
* Paciencia con religión
* Justificación de faltas online

# 24/10
* Portal de profesorado --> información 
* Subir sillas solo los viernes. No se limpia a 6ª
* Mascarillas 15 minutos comiendo
* Producto al papel y papel el mínimo
* Hasta el 25/09 no se habilita SIGAD
* Los padres podrán justificar las faltas. Correo corporativo del alumno para solicitar las claves.
* Paz nueva conserje
* Cambios de turno, solo hermanos. No se hacen cambios.
* Langaritas Francés en lugar de Música, no se acepta el cambio

# 01/10
* Predictiva: 40 minutos. 7/10. A las 16:00. 17:20 4B. UM1. Martes 1
* La 202 se va a haber afectada por el bachillerato.
* Reunión con padres: Online 
* Justificantes
* Centro de salud.
* Secretaría responsabilidad e familia, y el de vuelta (anexos)
* Los alumnos no pueden salir del centro --> Avisad (es decisión del consejo escolar, independientemente de la edad)
* Mascarilla en el recreo y guardias

# 15/10
* Micrófonos cerrados
* Se usa el classroom y correo electrónico, para que vean que se está pidiendo
* Sigad para justificar las faltas.
* Tokapp --> jefatura y secretaría
* Móviles --> Recreo ni fotos ni graban
* Abrigo para calefacción
* Mascarillas en los recreos. No se usan perchas. Extraescolares primer trimestre
* Titulación se está a la espera de instrucciones
* Base de datos covid la tenemos los tutores


Características el grupo: El grupo viene determinado por los itinerarios elegidos por el alumnado. El grupo de 4ºB son los alumnos que han cursado  FyQ y o bien Biología o Economía 

# 22/10

* Centro de salud, matrona --> preguntas sobre educación afectiva
* Charlas sobre discapacidad de una alumna de bachillerato
* Instrucciones ventanas: A primera cerrada. Circulación de 4 dedos. Ventanas del principio y final
* Charla de Francisco -->  Moto
* Salir del hoyo --> Web de orientación
* Recordar la normas de seguridad, sobre todo el intercambio
* Gaza -->

# 28/10
* Que no se queden en la puerta al inicio
* Devolver los libros

# 5/11
* Descargar el documento de cuarentena
* No evaluado en 1a y 2a
* Faltas justificadas, el tutor
* Sergio, David y Cora
    - cumplir matrícula
    - podrían ec, fyqb pero uno a turno 2. Expone solicita
* Expulsiones sigad
* guía justificar faltas --> web
* técnicas de estudio

# 12/11

* Evacuación, mapa y repaso
* Robo de móvil --> Si pueden no traer
* 1 y 2 ESO plan de animación tipo winwin
* Escanero --> Existe un correo 17/09 en Jefatura, un postit () 

# 19/11

* Violencia de genero (ver el termómetro 25/11 - Pepa y Pepe)
* Informar, carta de semipresencialidad. 
* Encuesta de igualdad --> Anónimo
* Ordenaores, que no tengan
* Mario Muñoz, faltas comprobar

# 27/11

* 18/12 evacuación 10m
* Es normativa 10m cada clase, cada 10 se duplica el Co2. El recreo entero. Una hora de calefacción más al día.
* Las notas irán por Sigad, se está barajando.

# 3/12

* Evaluaciones jueves el 17/12 18:15 de 4ESO. 1ESO martes.
* Simulacro de incendios
* Rellenad el Drive
* Boletín por sigad?

# 10/12

* Boletín por Sigad
* Martes 22/12 11:10 se suspende.
* No va a haber extraordinaria de Junio

# 07/01

* No más de 3 exámenes
* Gazza 15/01 
* Febrero visita familias profesional

# 14/01

* 25/01 Presencialidad
* 4c --> Dibujo

# 21/01

* Recordar las pruebas de acceso a grado medio
* 3 alumnas nuevas --> Refugiadas. Una de ellas tiene que ir a 4ºESO.
* Repasar las aulas para el martes

# 28/01

* La refugiada finalmente irá a 4E 1
* Diego se pilla la paternidad

# 04/02

* Encuesta 
* 3 semana espacio joven 19/02
* Carteles del covid 1 y 2. 
* Baños en los cambios
* Ampliación espacio recreo
* Interesados ciclos
* 3 y 4 semana visitas

# 11/02

* Pierden espacio los de 1 y 2 eso
* Recordad normas
* La semana que viene 17 ruta ciclos 
* 25/2 y 18/3 charla de proyecto hombre. A través del ayuntamiento
* Papel cuidado

# 18/02

* En mayo, ver lo de electrónica. 
* El patio cuando llueva
* Recordar el formulario de igualdad desde convivencia
* Orientación académica

# 25/02

* 4 de marzo, charla proyecto hombre a las 9:25, 18 de marzo.
* Drogas, juegos, etc.

# 04/03

* Maltrato psicológico
* 18/03 4B 17:40
* Previsión de derivaciones

# 11/03

* Derivaciones
* No hay septiembre

# 25/03

# 15/04

* Parlamento 22/4
* Orientación 7/5

# 06/05

* 13/05 reunión con alumnos
* Última semana con padre
* Reunión con Julia, si no han ido


# 13/05

* Programas perfiles diversos
* Desde el móvil, encuesta consumos
* Devolución de libros el día 16/6 todos
* Exámenes según calendario 16 y 17
* 11 de junio podrán ver las notas en Sigad
*

# 20/05

* Tormenta de ideas: Reciclaje
*